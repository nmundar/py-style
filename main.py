import sys, os

def print_color(message):
    print '\033[92m' + message + '\x1b[0m'
    print

print_color('================= PYTHON STYLE WORKSHOP =================')

names = [
'hrvoje', 'denis', 'boris', 'petra',
'martina',
'antonio', 'amalia', 'toni', 'neven',
'sasa', 'ana', 'dinko',
'senko'
]

surnames = [
'balija','solakovic', 'belonjek', 'rasic',
'dumancic',
'rabuzin','soucek', 'domazet', 'mundar',
'kalaba','vojnovic', 'mihovilovic',
'rasic',
]



def pair_name_and_surname(names, surnames):
# Task 1: make this more pythonic with zip()
# Task 2: is there something better than zip()?
    full_names = []
    n = min( len(names),len( surnames ) )
    for i in range( n ): # loop n times
        full_names.append((names[i], surnames[i]))
    return full_names

print_color('---- NAME PAIRS ----')

full_names = pair_name_and_surname(names, surnames)
for name, surname in full_names:
    print name,'-->',surname


def print_names_by_room(names):
# Task 3: make this also more pythonic (it's not c, we dont need all these indexes for loops)
    names_by_room = [names[0 : 4], names[ 4:5 ], names[5: 9], names[9 :12], names[ -1: ]]
    for i in range(len(names_by_room)):
        print 'room', str( i  + 1 ) + ':', # print room and number
        for name in names_by_room[ i ] :
        print name+ ',',;
        print

print_color('\n---- NAMES BY ROOM ----')

print_names_by_room(names)



def quicksort(lst):
    if not lst:
        return [] # return empty list
    return (quicksort([x for x in lst[1:] if x <  lst[0]])
        + [lst[0]] +
        quicksort([x for x in lst[1:] if x >= lst[0]])
    )

# Sort colors alphabeticaly
colors = ['red', 'green', 'blue', 'yellow', 'cyan', 'magenta', 'black', 'white', 'purple', 'gray', 'pink', 'brown', 'orange']
sorted_colors = quicksort(colors)

print_color('\n---- SORTED COLORS ---')
 print sorted_colors;
# Task 4: make this sorting more pythonic (python has something built in for this)



def reverse_list(list_to_reverse):
    reversed_list = []
    # Looping backwards
    for i in range(len(list_to_reverse)-1, -1, -1):
        reversed_list.append(list_to_reverse[i])
    return reversed_list

print_color('\n---- REVERSED SORTED COLORS ---')
    print reverse_list(sorted_colors)
# Task 5: make this more pythonic (I'll stop repeating that python has something built in...)


print_color('\n---- LOOPING DICTS ----')

# Looping over dictionaries
d = {'x':1, 'y': 2, 'z': 3, 'y': 4, 'a': 5, 'b': 6}
for key in d:
print key, 'corresponds to', d[key]

# Task 6: loop over dict and remove all even values (use del)
# Task 7: loop over dict and remove all even values (use dict comprehension)
def remove_even_from_dict(dictionary):
    pass

remove_even_from_dict(d)


print_color('\n---- COUNTING CARS ----')
# Count cars with dict
cars = ['vw', 'vw', 'renault', 'kia', 'ford', 'vw', 'citroen', 'suzuki', 'ford', 'mazda']


def dict_counter(items):
    d = {}
    for item in items:
        if not item in d:
            d[item]   = 0
        d[item] +=  1
    return d

print dict_counter(cars)
# Task 8: make this more pythonic (look into the standard library...)



print_color('\n---- UNPACKING ----')
# Unpacking sequences
point = 3,6,9
x = point[0]; y = point[1]; z = point[2]
print x, y, z

# Task 9: make this more pythonic (look up unpacking...)



print_color('\n---- CONCATENATING STRINGS ----')
# Concatenating strings
def concatenate_strings(strings):
    concatenated = strings[0]
    for s in strings[1:]:
        concatenated += ', ' + s
    return concatenated

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', '...']
print concatenate_strings(alphabet)
# Task 11: There has to be a better way...


def caesar(s, k, decode = False):
    if decode: k = 26 - k
    return "".join([chr((ord(i) - 65 + k) % 26 + 65)
                for i in s.upper()
                if ord(i) >= 65 and ord(i) <= 90 ])



print_color('\n---- TESTING ----')

import unittest


class TestMethods(unittest.TestCase):

    def test_pair_name_and_surname(self):
        self.assertEqual(
            pair_name_and_surname(['a', 'b', 'c'], ['x', 'y']),
            [('a', 'x'), ('b', 'y')]
        )

    def test_quicksort(self):
        self.assertEqual(
            quicksort(['red', 'green', 'blue', 'yellow']),
            ['blue', 'green', 'red', 'yellow']
        )

    def test_reverse_list(self):
        self.assertEqual(reverse_list(['a', 'b', 'c']), ['c', 'b', 'a'])

    def test_remote_event_from_dict(self):
        self.assertEqual(remove_even_from_dict({'a': 1, 'b': 2}), {'a': 1})

    def test_dict_counter(self):
        cars = [
            'vw', 'renault', 'kia',
            'vw', 'citroen', 'suzuki',
        ]
        self.assertEqual(
            dict_counter(cars),
            {'kia': 1, 'citroen': 1, 'suzuki': 1, 'renault': 1, 'vw': 2}
        )

    def test_concatenate_string(self):
        strings = ['a', 'b', 'c', '...']
        self.assertEqual(concatenate_strings(strings), 'a, b, c, ...')

    def test_caesar(self):
        self.assertEqual(caesar('asdf', 1), 'BTEG')
        self.assertEqual(caesar('asdf', 13), 'NFQS')
        self.assertEqual(caesar('qwerty', 13), 'DJREGL')


if __name__ == '__main__':
    unittest.main()
